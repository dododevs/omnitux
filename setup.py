"""
[build-system]
requires = ["setuptools"]

[project]
name = "Omnitux"
version = "1.2.2"
dependencies = [
    "pygobject",
    "pygame"
]
description = "The project aims to provide various educational activities around multimedia elements (images, sounds, texts)"
keywords = ["omnitux", "tux", "game", "education", "kids"]
readme = "README.md"

[project.urls]
Homepage = "https://omnitux.sourceforge.net/index.en.php"

[tool.setuptools]
include-package-data = false

[tool.setuptools.packages.find]
where = ["data", "src"]

[project.scripts]
omnitux = "omnitux:run"
"""

from setuptools import setup, find_packages

setup(
  name="Omnitux",
  version="1.2.2",
  description="The project aims to provide various educational activities around multimedia elements (images, sounds, texts)",
  long_description="The project aims to provide various educational activities around multimedia elements (images, sounds, texts)",
  keywords=["omnitux", "tux", "game", "education", "kids"],
  url="https://omnitux.sourceforge.net/index.en.php",
  include_package_data=True,
  package_data={
    "omnitux": ["data/**", "data/*", "*"]
  },
  install_requires=[
    "pygobject",
    "pygame"
  ],
  packages=["omnitux"],
  entry_points={
    "console_scripts": ["omnitux=omnitux:run"]
  },
)